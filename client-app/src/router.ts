import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Search from "./views/Search.vue";

Vue.use(Router);

export const constantRoutes = [
  {
    path: "/",
    name: "home",
    component: () =>
      import(/* webpackChunkName: "search.home" */ "./views/Search.vue"),
  },
  {
    path: "/search",
    name: "search",
    component: () =>
      import(/* webpackChunkName: "search" */ "./views/Search.vue"),
    meta: {
      title: "Search GitHub Users",
    },
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "./views/About.vue"),
  },
];

const createRouter = () =>
  new Router({
    mode: "history", // require service support
    routes: constantRoutes,
  });

const router = createRouter();
/**
 * Routes guarded by authentication
 * Update meta for each request
 * This callback runs before every route change, including on page load
 * This is considered a global guard (no access to 'this' scope, beforeResolve, afterEach) vs a route guard (beforeEnter)
 * which does have access to 'this' scope
 * Component guards are beforeRouteEnter (no access to this scope), beforeRouteUpdate & beforeRouteLeave
 * Example: https://medium.com/js-dojo/how-to-implement-route-guard-in-vue-js-9929c93a13db
 * @param {*} to
 * @param {*} from
 * @param {*} next
 */
router.beforeEach((to, from, next) => {
  /**
   * TODO: Look to use store instead of localStorage.getItem('LoggedUser')
   */
  const loggedIn = localStorage.getItem("user");
  if (to.matched.some((record) => record.meta.auth) && !loggedIn)
    next({
      name: "login",
    });
  else next();
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Skip rendering meta tags if there are none.
  if (!nearestWithMeta) return next();

  next();
});

export function resetRouter() {
  const newRouter = createRouter();
}

export default router;
